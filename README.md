# step-project-forkio

Проeкт сделали:
- Яремченко Максим
- Сергей Барсуков

Использованные технологии:
- Gulp
- SCSS
- JavaScript
- BEM

Задачи участников: 
Яремченко Максим (student1)
- Сделал Gulp сборку
- Сверстал шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана).
- Сверстал секцию People Are Talking About Fork.

Сергей Барсуков (student2)
- Сверстал блок Revolutionary Editor.
- Светстал секцию Here is what you get.
- Сверстал секцию Fork Subscription Pricing.